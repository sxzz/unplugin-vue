# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0](https://github.com/sxzz/unplugin-vue/compare/v0.2.2...v1.0.0) (2022-03-01)


### Features

* support node 12 ([aff7c06](https://github.com/sxzz/unplugin-vue/commit/aff7c06b6a2fd9ae76df7cc9711e10be1e971b5a))


### Bug Fixes

* adjust vue template sourcemap ([1fcee44](https://github.com/sxzz/unplugin-vue/commit/1fcee443638748f55d98ee1cf24fee3ae5b275d0))
* build ([194ef34](https://github.com/sxzz/unplugin-vue/commit/194ef34d22bceedc850098c13f9f8050040ec675))
* make cssm code tree shakeable ([b2a273b](https://github.com/sxzz/unplugin-vue/commit/b2a273b37ce514b693eed2952746fc5b06ab72bc))
* setup jsx script no hmr ([582dfdd](https://github.com/sxzz/unplugin-vue/commit/582dfdd2df157e603e92d1829ee3cd5401bdd8bf))

### [0.2.2](https://github.com/sxzz/unplugin-vue/compare/v0.2.1...v0.2.2) (2022-01-21)


### Bug Fixes

* **build:** node_modules hoist ([69aac15](https://github.com/sxzz/unplugin-vue/commit/69aac15104a32ea645fc79de1f301cf3141391b4))

### [0.2.1](https://github.com/sxzz/unplugin-vue/compare/v0.2.0...v0.2.1) (2022-01-21)


### Features

* add webpack example ([3de64a5](https://github.com/sxzz/unplugin-vue/commit/3de64a54310fb2340867608dd5dd3c80f38ea1ea))


### Bug Fixes

* esbuild sourcemap ([319a600](https://github.com/sxzz/unplugin-vue/commit/319a60068731142a1f1171eecd4654c55d12cdfc))

## [0.2.0](https://github.com/sxzz/unplugin-vue/compare/v0.1.7...v0.2.0) (2022-01-19)

### [0.1.7](https://github.com/sxzz/unplugin-vue/compare/v0.1.6...v0.1.7) (2022-01-19)


### Features

* support src when rollup ([77ae387](https://github.com/sxzz/unplugin-vue/commit/77ae3877940a4e066604d39b850f685f4dd105c4))

### [0.1.6](https://github.com/sxzz/unplugin-vue/compare/v0.1.5...v0.1.6) (2022-01-19)


### Features

* add vite example ([fd176a4](https://github.com/sxzz/unplugin-vue/commit/fd176a4f37e5142ae836532f5f938c9e4ccf230d))


### Bug Fixes

* package slash ([d9f0a28](https://github.com/sxzz/unplugin-vue/commit/d9f0a280ca58275be6b8da18bb15ef7fb1d1d9f6))

### [0.1.5](https://github.com/sxzz/unplugin-vue/compare/v0.1.4...v0.1.5) (2022-01-19)


### Bug Fixes

* **build:** tsup baseUrl ([7ba36e9](https://github.com/sxzz/unplugin-vue/commit/7ba36e93dfce1e5f7e8e58fc661db909d5d525cb))

### [0.1.4](https://github.com/sxzz/unplugin-vue/compare/v0.1.3...v0.1.4) (2022-01-19)


### Features

* support vite hmr ([c934e9f](https://github.com/sxzz/unplugin-vue/commit/c934e9f1871f93f73f0650cd25029afa1465a8c8))

### [0.1.3](https://github.com/sxzz/unplugin-vue/compare/v0.1.2...v0.1.3) (2022-01-19)


### Bug Fixes

* types ([c53f8da](https://github.com/sxzz/unplugin-vue/commit/c53f8dae633162b969790872a6cbbd4999115f4a))

### [0.1.2](https://github.com/sxzz/unplugin-vue/compare/v0.1.1...v0.1.2) (2022-01-19)

### [0.1.1](https://github.com/sxzz/unplugin-vue/compare/v0.1.0...v0.1.1) (2022-01-19)


### Features

* add examples ([3934bec](https://github.com/sxzz/unplugin-vue/commit/3934bec126e836b6565ce28a3517a64a39ba9098))

## [0.1.0](https://github.com/sxzz/unplugin-vue/compare/v0.0.2...v0.1.0) (2022-01-19)


### Features

* support unplugin ([d033042](https://github.com/sxzz/unplugin-vue/commit/d0330426611035186a87c4de794a39c595938a6c))

### 0.0.2 (2022-01-19)

### Features

- add build script ([ca145fa](https://github.com/sxzz/unplugin-vue/commit/ca145fa52deb821b0315eea35f3e10fe7fae53f1))
- add sourceMap and root option ([5565fe0](https://github.com/sxzz/unplugin-vue/commit/5565fe09d28d1a4cea5007aba4bfc25cc9c5b5c7))
- init project ([a61c30d](https://github.com/sxzz/unplugin-vue/commit/a61c30d6c46ad070e40284d1d61bb75d56cdca66))
