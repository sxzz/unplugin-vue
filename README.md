# unplugin-vue [![npm](https://img.shields.io/npm/v/unplugin-vue.svg)](https://npmjs.com/package/unplugin-vue)

[![Unit Test](https://github.com/sxzz/unplugin-vue/actions/workflows/unit-test.yml/badge.svg)](https://github.com/sxzz/unplugin-vue/actions/workflows/unit-test.yml)

Transform Vue 3 SFC to JavaScript.

## Features

- ⚡️ Supports Vite, Webpack, Vue CLI, Rollup, esbuild and more, powered by [unplugin](https://github.com/unjs/unplugin).
- ✨ Supports `<script setup>`
- 💚 Supports [Reactivity Transform](https://github.com/vuejs/rfcs/discussions/369)

## Who is using

- [vue-components-lib-seed](https://github.com/zouhangwithsweet/vue-components-lib-seed) - A vue3.0 components library template.

## Alternatives

- [@vitejs/plugin-vue](https://github.com/vitejs/vite/tree/main/packages/plugin-vue) - For Vite and Vue 3.
- [vite-plugin-vue2](https://github.com/underfin/vite-plugin-vue2) - For Vite and Vue 2.
- ~~[rollup-plugin-vue](https://github.com/vuejs/rollup-plugin-vue)~~ - ⚠️ no longer maintained.
- [vue-loader](https://github.com/vuejs/vue-loader) - For Webpack.
- [esbuild-plugin-vue](https://github.com/egoist/esbuild-plugin-vue) - For ESBuild and Vue 3.
- [esbuild-vue](https://github.com/apeschar/esbuild-vue) - For ESBuild and Vue 2.

## Credits

- [Vite](https://github.com/vitejs/vite) - Next generation frontend tooling. It's fast!
- [unplugin](https://github.com/unjs/unplugin) - Unified plugin system for Vite, Rollup, Webpack, and more

## Thanks

Thanks to [Vite](https://github.com/vitejs/vite). This project is inherited from [@vitejs/plugin-vue@2.2.3](https://github.com/vitejs/vite/tree/main/packages/plugin-vue).

## License

[MIT](./LICENSE) License © 2022 [三咲智子](https://github.com/sxzz)
